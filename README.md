# TypeCode.com #

Meteor web site to help practice touch typing for developers. Users can create lessons of any remote code file to practice touch typing and track their results. It was inspired by [typing.io](https://typing.io) and [typingclub.com](http://typingclub.com).

Live site: https://type-code.herokuapp.com.

Blog series: http://blog.federicosilva.net/search/label/type-code.com.

### What is it build with? ###
* Node.js
* Meteor.js
* React.js
* Monaco.js
* Google Auth
* MongoDB
* BlazeCSS

### How to build it? ###
* Install meteor.js
* Clone repo
* Run 'npm install' from root
* Run 'meteor' from root

### How to setup Google Auth? ###
* Activate the [Google Auth API](https://console.developers.google.com)
* Create a dev-settings.json file with:
```
{
    "clientId": "<Your client Id>",
    "secret": "<Your client secret>"
}
```
* Run 'meteor --settings dev-settings.json' from root.

### How to run tests? ###
The only component that has tests is the core typing engine. You can run tests from the console or from the web UI:

* Console:
    * Run 'npm run test' from root. Mocha results will appear in console.
* Web UI:
    * Run 'npm run tes-browser' from root. Navigate to http://localhost:3000 to run the same tests from browser.

### Attributions ###
* Some icons made by [Vectors Market](http://www.flaticon.com/authors/vectors-market) from [www.flaticon.com](www.flaticon.com).