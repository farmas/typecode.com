export const CURSOR_WAITING = 'CURSOR_WAITING';
export const CURSOR_TYPING = 'CURSOR_TYPING';
export const CURSOR_BACKSPACE = 'CURSOR_BACKSPACE';
export const CURSOR_ERROR = 'CURSOR_ERROR';
export const CURSOR_EOL = 'CURSOR_EOL';
export const CURSOR_EOF = 'CURSOR_EOF';
export const CHAR_EOL = 'CHAR_EOL';
export const CHAR_EOF = 'CHAR_EOF';
