import * as Constants from './constants';
import ObjectAssign from 'object-assign';
import s from 'underscore.string';

const DEFAULT_CHAR_COUNT = 200;

export default class Engine {
    constructor() {
        this.code = [];
        this._resetState();
    }

    _resetState() {
        this.endCursor = null;
        this.cursor = {
            row: 0,
            col: 0,
            char: null,
            type: Constants.CURSOR_WAITING
        };
        this.typeError = null;
        this.history = [];
        this.getCommentsForRowFunc = null;
        this.comments = {};
        this.errorMap = {};
        this.totalKeysTyped = 0;
        this.totalErrors = 0;
    }

    getState() {
        return this._buildResult(this.cursor, this.typeError);
    }

    getFormattedCode() {
        return this.code.join('\n');
    }

    loadCode(code) {
        if (typeof code !== 'string') {
            throw new Error('Code must be a string');
        }

        this.code = code.split('\n').map(line => s.rtrim(line) + ' ');

        return this;
    }

    start(row = 0, charCount = DEFAULT_CHAR_COUNT, getCommentsForRowFunc = null) {
        this._resetState();

        this.getCommentsForRowFunc = getCommentsForRowFunc;

        let nextCursor = this._getFirstCursorFromRow(this.code, row);
        let error = null;

        if (nextCursor.char === Constants.CHAR_EOF) {
            error = new Error('No valid character to start typing.');
        }

        this.cursor.row = nextCursor.row;
        this.cursor.col = nextCursor.col;
        this.cursor.char = nextCursor.char;
        this.cursor.type = Constants.CURSOR_WAITING;
        this.endCursor = this._getEndCursor(this.code, nextCursor, charCount);

        //console.log('startCursor', this.cursor, 'endCursor', this.endCursor);
        var result = this._buildResult(this.cursor);
        result.endCursor = this.endCursor;
        result.rowCount = this.code.length;
        result.error = error;

        return result;
    }

    backspace() {
        if (!this._isComplete(this.cursor, this.typeError)) {
            this.totalKeysTyped++;

            var oldCursor = this.history.pop();
            if (oldCursor) {
                if (this.typeError && this.typeError.startRow == oldCursor.row && this.typeError.startCol == oldCursor.col) {
                    // we have deleted the last error.
                    this.typeError = null;
                }

                // assign the current cursor
                this.cursor = ObjectAssign({}, oldCursor);

                if (this.cursor.type === Constants.CURSOR_WAITING) {
                    this.cursor.type = Constants.CURSOR_TYPING;
                }
            }
        }

        var result = this._buildResult(this.cursor, this.typeError);

        //console.log('backspace', 'result', result);
        return result;
    }

    type(char) {
        if (!this._isComplete(this.cursor, this.typeError)) {
            this.totalKeysTyped++;

            // 1) check if the character typed is wrong.
            if (char !== this.cursor.char) {
                this.totalErrors++;

                if (!this.typeError) {
                    // This is the first wrong character (there may be more after this), mark
                    //   the current cursor as the beginning of the error.

                    var keyError = this.errorMap[this.cursor.char] || { count: 0, typedChars: {} };
                    keyError.count ++;
                    keyError.typedChars[char] = (keyError.typedChars[char] || 0) + 1;
                    this.errorMap[this.cursor.char] = keyError;

                    this.typeError = {
                        startRow: this.cursor.row,
                        startCol: this.cursor.col,
                        type: Constants.CURSOR_ERROR
                    }
                }
            }

            // 2) save the current cursor in history
            this.history.push(ObjectAssign({}, this.cursor));

            // 3) Find the position for the next cursor
            let nextCursor = this._getNextCursor(this.code, this.cursor);

            // 4) set the new cursor
            this.cursor.row = nextCursor.row;
            this.cursor.char = nextCursor.char;
            this.cursor.col = nextCursor.col;

            if (!!this.typeError) {
                this.cursor.type = Constants.CURSOR_BACKSPACE;
            } else if (this._isComplete(nextCursor)) {
                this.cursor.type = Constants.CURSOR_EOF;
            } else if (nextCursor.char === Constants.CHAR_EOL) {
                this.cursor.type = Constants.CURSOR_EOL;
            } else {
                this.cursor.type = Constants.CURSOR_TYPING;
            }
        }

        let result = this._buildResult(this.cursor, this.typeError);

        //console.log('typed', char, 'result', result);
        return result;
    }

    static getDefaultResult() {
         return {
            isComplete: false,
            isLastBlock: false,
            error: null,
            cursor: {
                row: 0,
                col: 0,
                char: null,
                type: Constants.CURSOR_WAITING
            },
            typeError: null,
            stats: {
                totalChars: 0,
                totalKeysTyped: 0,
                totalErrors: 0,
                errorMap: {},
                currentLine: 0,
                totalLines: 0

            }
        };
    }

    _buildResult(cursor, typeError, error) {
        let isComplete = this._isComplete(cursor, typeError);
        let currentLine = cursor.row + 1;
        let isLastBlock = isComplete
            && (currentLine >= this.code.length || this._getFirstCursorFromRow(this.code, currentLine).char === Constants.CHAR_EOF);

        return {
            isComplete: isComplete,
            isLastBlock: isLastBlock,
            error: error,
            cursor: cursor,
            typeError: typeError,
            stats: {
                totalChars: this.history.length,
                totalKeysTyped: this.totalKeysTyped,
                totalErrors: this.totalErrors,
                errorMap: this.errorMap,
                currentLine: currentLine,
                lastBlockLine: this.endCursor.row + 1,
                totalLines: this.code.length,
            }
        };
    }

    _isEOF(cursor) {
        let isEndCursor = this.endCursor && this.endCursor.row === cursor.row && this.endCursor.col === cursor.col;
        return isEndCursor || cursor.char === Constants.CHAR_EOF;
    }

    _isComplete(cursor, typeError) {
        return !typeError && this._isEOF(cursor);
    }

    _getCommentsForRow(row) {
        var key = row.toString();

        if (!this.comments.hasOwnProperty(key) && this.getCommentsForRowFunc) {
            this.comments[key] = this.getCommentsForRowFunc(row);
        }

        return this.comments[key];
    }

    _getComment(row, col) {
        var comments = this._getCommentsForRow(row);
        return comments && comments.find(comment => comment.startIndex === col);
    }

    _getNextCursor(code, cursor) {
        return cursor.char === Constants.CHAR_EOL ?
             this._getFirstCursorFromRow(code, cursor.row + 1)
           : this._getNextCursorInRow(code, cursor.row, cursor.col);
    }

    _getEndCursor(code, startCursor, minCount) {
        let endCursor = startCursor;
        let count = 1;

        while (count < minCount && endCursor.char !== Constants.CHAR_EOF) {
            endCursor = this._getNextCursor(this.code, endCursor);
            count ++;

            //console.log('endCursor', endCursor);
        }

        if (endCursor.char !== Constants.CHAR_EOF && endCursor.char !== Constants.CHAR_EOL) {
            // go to the end of the line
            let line = code[endCursor.row];
            endCursor.col = line.length - 1;
            endCursor.char = Constants.CHAR_EOF;
        }

        return endCursor;
    }

    _getNextCursorInRow(code, row, col) {
        let nextCol = col + 1;
        let nextRow = row;
        let nextChar = null;

        while(!nextChar && (nextRow < code.length)) {
            let line = code[nextRow];
            let comment = this._getComment(nextRow, nextCol);

            if (comment) {
                if (!comment.endIndex || (comment.endIndex + 1) >= line.length) {
                    // The rest of the line is a comment, go to the end and wait for eol
                    nextCol = line.length - 1;
                    nextChar = nextRow >= (code.length - 1) ? Constants.CHAR_EOF : Constants.CHAR_EOL;
                } else {
                    // Start of a block comment, go to the next char after it (even if it is a space).
                    nextCol = comment.endIndex;
                    nextChar = line[nextCol];
                }
            } else if ((nextCol + 1) >= line.length) {
                // there are no more characters in this line, the next char is either eol or eof.
                nextChar = nextRow >= (code.length - 1) ? Constants.CHAR_EOF : Constants.CHAR_EOL;
            } else {
                nextChar = line[nextCol];
            }

            //console.log('_getNextCursor', 'nextChar:', nextChar, 'nextRow:', nextRow, 'nextCol:', nextCol, 'line.length:', line.length);
        }

        return {
            row: nextRow,
            col: nextCol,
            char: nextChar
        };
    }

    _getFirstCursorFromRow(code, row) {
        let nextCol = 0;
        let nextRow = row;
        let nextChar = null;

        while(!nextChar && (nextRow < code.length)) {
            let line = code[nextRow];
            let comment = this._getComment(nextRow, nextCol);

            if ((nextCol + 1) >= line.length) {
                // reached the end of the line, go to the next line.
                nextRow ++;
                nextCol = 0;
            } else if (comment) {
                if (!comment.endIndex) {
                    // the rest of the line is a comment, go to the next line
                    nextRow++;
                    nextCol = 0;
                } else {
                    // start of a block comment, go the next char after the comment
                    nextCol = comment.endIndex;
                }
            } else {
                nextChar = line[nextCol].trim();
                if (!nextChar) {
                    // this is a space character, go to the next char
                    nextCol ++;
                }
            }

            //console.log('_getFirstCursorFromRow', 'nextChar:', nextChar, 'nextRow:', nextRow, 'nextCol:', nextCol, 'line.length:', line.length);
        }

        if (!nextChar) {
            nextRow = code.length - 1;
            nextCol = code[nextRow].length - 1;
            nextChar = Constants.CHAR_EOF;
        }

        return {
            row: nextRow,
            col: nextCol,
            char: nextChar
        };
    }
}
