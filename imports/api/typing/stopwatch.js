/*
Adapted from: https://gist.github.com/electricg/4372563

Copyright (c) 2010-2015 Giulia Alfonsi <electric.g@gmail.com>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
export default class Stopwatch {

    constructor() {
        this._startAt = 0; // Time of last start / resume. (0 if not running)
        this._lapTime = 0; // Time on the clock when last stopped in milliseconds
        this._timer = null;
    }

    _now() {
        return (new Date()).getTime();
    }

    start(callback) {
        this._startAt = this._startAt ? this._startAt : this._now();

        window.clearInterval(this._timer);
        this._timer = window.setInterval(() => {
            var time = this.time();
            var formattedTime = this._formatTime(time);
            callback(time, formattedTime);
        }, 1000);
    }

    stop() {
        // If running, update elapsed time otherwise keep it
        this._lapTime = this._startAt ? this._lapTime + this._now() - this._startAt : this._lapTime;
        this._startAt = 0; // Paused

        window.clearInterval(this._timer);
    }

    reset() {
        this._lapTime = this._startAt = 0;
        window.clearInterval(this._timer);
    }

    time() {
        return this._lapTime + (this._startAt ? this._now() - this._startAt : 0);
    }

    _formatTime(time) {
        var m = 0;
        var s = 0;
        var newTime = '';

        time = time % (60 * 60 * 1000);
        m = Math.floor( time / (60 * 1000) );
        time = time % (60 * 1000);
        s = Math.floor( time / 1000 );

        newTime = this._pad(m, 2) + ':' + this._pad(s, 2);
        return newTime;
    }

    _pad(num, size) {
        var s = "0000" + num;
        return s.substr(s.length - size);
    }
}