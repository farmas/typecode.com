import chai from 'chai';
import Engine from './engine';
import * as Constants from './constants';

var should = chai.should();

function assertCursor(cursor, row, col, type) {
    cursor.row.should.equal(row, 'Cursor row');
    cursor.col.should.equal(col, 'Cursor col');
    if (!!type) {
        cursor.type.should.equal(type, 'Cursor type');
    }
}

function assertError(typeError, startRow, startCol) {
    typeError.startRow.should.equal(startRow);
    typeError.startCol.should.equal(startCol);
}

Array.prototype.getByIndex = function(index) {
    return this[index];
}

describe('When finished', function() {
    let engine;

    beforeEach(function() {
        engine = new Engine();
    });

    it('will return stats with total number of characters', function() {
        engine.loadCode('abc').start();

        engine.type('a');
        engine.type('b');
        var result = engine.type('c');

        result.isComplete.should.equal(true);
        result.stats.totalChars.should.equal(3);
    });

    it('will return stats with total number of keys typed', function() {
        engine.loadCode('abc').start();

        engine.type('a');
        engine.type('b');
        var result = engine.type('c');

        result.stats.totalKeysTyped.should.equal(3);
    });

    it('will return stats with total number of keys typed including backspaces', function() {
        engine.loadCode('abc').start();

        engine.type('a');
        engine.type('x');
        engine.backspace();
        engine.type('b')
        var result = engine.type('c');

        result.isComplete.should.equal(true);
        result.stats.totalKeysTyped.should.equal(5);
    });

    it('will return stats with heat map of all error keys', function() {
        engine.loadCode('ab').start();

        engine.type('a');
        engine.type('x');
        engine.backspace();
        engine.type('x');
        engine.backspace();
        engine.type('y');
        engine.backspace();
        var result = engine.type('b')

        result.isComplete.should.equal(true);
        result.stats.totalKeysTyped.should.equal(8);
        result.stats.totalErrors.should.equal(3);
        result.stats.errorMap.b.count.should.equal(3);
        result.stats.errorMap.b.typedChars.x.should.equal(2);
        result.stats.errorMap.b.typedChars.y.should.equal(1);
    });

    it('will return heat map that includes the first wrong key', function() {
        engine.loadCode('ab').start();

        engine.type('a');
        engine.type('x');
        engine.type('y');
        engine.backspace();
        engine.backspace();
        var result = engine.type('b')

        result.isComplete.should.equal(true);
        result.stats.totalKeysTyped.should.equal(6);
        result.stats.totalErrors.should.equal(2);
        result.stats.errorMap.b.count.should.equal(1);
        result.stats.errorMap.b.typedChars.x.should.equal(1);
    });

    it('will return stats with total number of errors', function() {
        engine.loadCode('ab').start();

        engine.type('a');
        engine.type('x');
        engine.backspace();
        engine.type('x');
        engine.backspace();
        engine.type('y');
        engine.backspace();
        var result = engine.type('b')

        result.isComplete.should.equal(true);
        result.stats.totalErrors.should.equal(3);
    });
});

describe('When backspace', function() {
    let engine;

    beforeEach(function() {
        engine = new Engine();
    });

    it('should move cursor to previous indented line if reached start of line', function() {
        var result = engine.loadCode('  a\nbc').start();

        engine.type('a');
        engine.type(Constants.CHAR_EOL);
        result = engine.type('b');
        assertCursor(result.cursor, 1, 1, Constants.CURSOR_TYPING);

        result = engine.backspace();
        assertCursor(result.cursor, 1, 0, Constants.CURSOR_TYPING);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 3, Constants.CURSOR_EOL);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 2, Constants.CURSOR_TYPING);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 2, Constants.CURSOR_TYPING);
    });

    it('should move cursor to previous line if reached start of line', function() {
        engine.loadCode('a\nb\nc\nd').start();

        engine.type('a');
        engine.type(Constants.CHAR_EOL);
        engine.type('b');
        engine.type(Constants.CHAR_EOL);
        var result = engine.type('c');
        assertCursor(result.cursor, 2, 1, Constants.CURSOR_EOL);

        result = engine.backspace();
        assertCursor(result.cursor, 2, 0, Constants.CURSOR_TYPING);

        result = engine.backspace();
        assertCursor(result.cursor, 1, 1, Constants.CURSOR_EOL);

        result = engine.backspace();
        assertCursor(result.cursor, 1, 0, Constants.CURSOR_TYPING);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_EOL);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 0, Constants.CURSOR_TYPING);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 0, Constants.CURSOR_TYPING);
    });

    it('should move cursor to the left clearing errors', function() {
        engine.loadCode('abc').start();

        engine.type('x');
        engine.type('x');
        engine.type('x');

        let result = engine.backspace();
        result.isComplete.should.equal(false);
        assertCursor(result.cursor, 0, 2, Constants.CURSOR_BACKSPACE);
        assertError(result.typeError, 0, 0);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_BACKSPACE);
        assertError(result.typeError, 0, 0);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 0, Constants.CURSOR_TYPING);
        should.not.exist(result.typeError);
    });

    it('should prevent backspace if complete', function() {
        engine.loadCode('abc').start();

        engine.type('a');
        engine.type('b');
        engine.type('c');

        var result = engine.backspace();
        result.isComplete.should.equal(true);
        assertCursor(result.cursor, 0, 3, Constants.CURSOR_EOF);
    });

    it('should prevent backspace when reaching beginning', function() {
        engine.loadCode('abc').start();

        var result = engine.backspace();
        assertCursor(result.cursor, 0, 0, Constants.CURSOR_WAITING);

        engine.type('a');
        engine.type('b');

        engine.backspace();
        engine.backspace();
        result = engine.backspace();
        assertCursor(result.cursor, 0, 0, Constants.CURSOR_TYPING);
    });

    it('should move cursor to the left', function() {
        engine.loadCode('abcde').start();

        engine.type('a');
        engine.type('b');
        engine.type('c');

        var result = engine.backspace();
        assertCursor(result.cursor, 0, 2, Constants.CURSOR_TYPING);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_TYPING);

        result = engine.backspace();
        assertCursor(result.cursor, 0, 0, Constants.CURSOR_TYPING);
    });

    describe('and code has comments', function() {
        it('will skip comments after user back tracks', function() {
            var comments = [
                [{ startIndex: 1, endIndex: null }],
                []
            ];

            engine.loadCode('a//b\ncd').start(0, 200, comments.getByIndex.bind(comments));

            engine.type('a');
            var result = engine.type(Constants.CHAR_EOL);
            assertCursor(result.cursor, 1, 0, Constants.CURSOR_TYPING);

            result = engine.backspace();
            assertCursor(result.cursor, 0, 4, Constants.CURSOR_EOL);

            result = engine.backspace();
            assertCursor(result.cursor, 0, 0, Constants.CURSOR_TYPING);

            engine.type('a');
            result = engine.type(Constants.CHAR_EOL);
            assertCursor(result.cursor, 1, 0, Constants.CURSOR_TYPING);
        });
    });
});

describe('When incorrect typing', function() {
    let engine;

    beforeEach(function() {
        engine = new Engine();
    });

    it('should move cursor to next indented line with errors', function() {
        var result = engine.loadCode('  a\n  bc').start();
        assertCursor(result.cursor, 0, 2, Constants.CURSOR_WAITING);

        result = engine.type(' ');
        assertCursor(result.cursor, 0, 3, Constants.CURSOR_BACKSPACE);
        assertError(result.typeError, 0, 2);

        result = engine.type(' ');
        assertCursor(result.cursor, 1, 2, Constants.CURSOR_BACKSPACE);
        assertError(result.typeError, 0, 2);

        result = engine.type('a');
        assertCursor(result.cursor, 1, 3, Constants.CURSOR_BACKSPACE);
        assertError(result.typeError, 0, 2);
    });

    it('should move cursor to the right and mark previous characters as error', function() {
        engine.loadCode('test').start();

        var result = engine.type('x');
        result.isComplete.should.equal(false);
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_BACKSPACE);
        result.typeError.should.exist;
        assertError(result.typeError, 0, 0);

        result = engine.type('x');
        result.isComplete.should.equal(false);
        assertCursor(result.cursor, 0, 2, Constants.CURSOR_BACKSPACE);
        assertError(result.typeError, 0, 0);
    });

    it('should move cursor to the next line if typing wrong char at the end of the line', function() {
        engine.loadCode('f\na').start();

        var result = engine.type('f');
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_EOL);
        should.not.exist(result.typeError);

        result = engine.type('x');
        result.isComplete.should.equal(false);
        assertCursor(result.cursor, 1, 0, Constants.CURSOR_BACKSPACE);
        assertError(result.typeError, 0, 1);
    });

    it('should not end if reaching eol when there are no more characters in subsequent lines', function() {
        engine.loadCode('fo\n   \n\n   ').start();

        engine.type('f');
        var result = engine.type('x');
        result.isComplete.should.equal(false);
        assertError(result.typeError, 0, 1);
        assertCursor(result.cursor, 0, 2, Constants.CURSOR_BACKSPACE);
    });

    it('should not end if reaching eof if typing last character wrong', function() {
        engine.loadCode('foo').start();

        engine.type('f');
        engine.type('o');
        var result = engine.type('x');
        result.isComplete.should.equal(false);
        assertError(result.typeError, 0, 2);
        assertCursor(result.cursor, 0, 3, Constants.CURSOR_BACKSPACE);
    });

    it('should prevent further typing if reaching eof', function() {
        engine.loadCode('foo').start();

        engine.type('f');
        engine.type('o');
        engine.type('x');
        engine.type('x');
        var result = engine.type('x');

        result.isComplete.should.equal(false);
        assertError(result.typeError, 0, 2);
        assertCursor(result.cursor, 0, 5, Constants.CURSOR_BACKSPACE);
    });
});

describe('When correct typing', function() {
    let engine;

    beforeEach(function() {
        engine = new Engine();
    });

    it('should end if there are no character in subsequent lines when typing return at the end of the line', function() {
        engine.loadCode('f\n  \n\n   ').start();

        var result = engine.type('f');
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_EOL);

        result = engine.type(Constants.CHAR_EOL);
        result.isComplete.should.equal(true);
        assertCursor(result.cursor, 3, 0, Constants.CURSOR_EOF);
    });

    it('should move cursor to first indented character of first subsequent line when typing return at the end of the line', function() {
        engine.loadCode('f\n  \n\n   a').start();

        var result = engine.type('f');
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_EOL);

        result = engine.type(Constants.CHAR_EOL);
        assertCursor(result.cursor, 3, 3, Constants.CURSOR_TYPING);
    });

    it('should move cursor to first indented character of next line when typing return at the end of the line', function() {
        engine.loadCode('f\n   a').start();

        var result = engine.type('f');
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_EOL);

        result = engine.type(Constants.CHAR_EOL);
        assertCursor(result.cursor, 1, 3, Constants.CURSOR_TYPING);
    });

    it('should move cursor to the next line when typing return at the end of the line', function() {
        engine.loadCode('f\na').start();

        var result = engine.type('f');
        result.isComplete.should.equal(false);
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_EOL);
        should.not.exist(result.typeError);

        result = engine.type(Constants.CHAR_EOL);
        assertCursor(result.cursor, 1, 0, Constants.CURSOR_TYPING);
    });

    it('should prevent further typing if reached the end', function() {
        engine.loadCode('f').start();

        var result = engine.type('f');
        result.isComplete.should.equal(true);
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_EOF);

        result = engine.type('o');
        result.isComplete.should.equal(true);
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_EOF);
    });

    it('should move the cursor to the right', function() {
        engine.loadCode('foo').start();

        var result = engine.type('f');
        result.isComplete.should.equal(false);
        assertCursor(result.cursor, 0, 1, Constants.CURSOR_TYPING);

        result = engine.type('o');
        result.isComplete.should.equal(false);
        assertCursor(result.cursor, 0, 2, Constants.CURSOR_TYPING);

        result = engine.type('o');
        result.isComplete.should.equal(true);
        assertCursor(result.cursor, 0, 3, Constants.CURSOR_EOF);
    });

    describe('and char count', function() {
        it('will end if reaching end cursor', function() {
            engine.loadCode('abc\ndef').start(0, 2);

            engine.type('a');
            engine.type('b');

            var result = engine.type('c');
            assertCursor(result.cursor, 0, 3, Constants.CURSOR_EOF);

            result = engine.type(Constants.CHAR_EOL);
            assertCursor(engine.cursor, 0, 3, Constants.CURSOR_EOF);
        });
    });

    describe('and code has comments', function() {
        it('will skip single line comment', function() {
            var comments = [
                [{ startIndex: 1, endIndex: undefined }],
                []
            ];

            engine.loadCode('a//b\ncd').start(0, 200, comments.getByIndex.bind(comments));

            var result = engine.type('a');
            assertCursor(result.cursor, 0, 4, Constants.CURSOR_EOL);

            var result = engine.type(Constants.CHAR_EOL);
            assertCursor(result.cursor, 1, 0, Constants.CURSOR_TYPING);

        });

        it('will skip all single line comments', function() {
            var comments = [
                [{ startIndex: 1, endIndex: undefined }],
                [{ startIndex: 3, endIndex: undefined }],
                []
            ];

            engine.loadCode('a//b\n   //c   \n   de').start(0, 200, comments.getByIndex.bind(comments));

            var result = engine.type('a');
            assertCursor(result.cursor, 0, 4, Constants.CURSOR_EOL);

            result = engine.type(Constants.CHAR_EOL);
            assertCursor(result.cursor, 2, 3, Constants.CURSOR_TYPING);
        });

        it('will skip block comments on same line', function() {
            var comments = [
                [{ startIndex: 1, endIndex: 6 }, { startIndex: 7, endIndex: 12 }]
            ];

            engine.loadCode('a/*b*/c/*d*/   ef').start(0, 200, comments.getByIndex.bind(comments));

            var result = engine.type('a');
            assertCursor(result.cursor, 0, 6, Constants.CURSOR_TYPING);

            result = engine.type('c');
            assertCursor(result.cursor, 0, 12, Constants.CURSOR_TYPING);
        });

        it('will skip all comments on subsequent lines', function() {
            var comments = [
                [],
                [{ startIndex: 0, endIndex: 5 }],
                [{ startIndex: 3, endIndex: 8 }],
                [{ startIndex: 0, endIndex: undefined }],
                []
            ];

            engine.loadCode('a\n/*b*/\n   /*c*/  \n//d\n   ef').start(0, 200, comments.getByIndex.bind(comments));

            engine.type('a');
            var result = engine.type(Constants.CHAR_EOL);
            assertCursor(result.cursor, 4, 3, Constants.CURSOR_TYPING);
        });

        it('will will reach eof if comment block is at the end of file.', function() {
            var comments = [
                [{ startIndex: 1, endIndex: 6 }]
            ];

            engine.loadCode('a/*b*/').start(0, 200, comments.getByIndex.bind(comments));

            var result = engine.type('a');
            assertCursor(result.cursor, 0, 6, Constants.CURSOR_EOF);
        });

        it('will will reach eof if line comment is at the end of file.', function() {
            var comments = [
                [{ startIndex: 1, endIndex: null }]
            ];

            engine.loadCode('a//b').start(0, 200, comments.getByIndex.bind(comments));

            var result = engine.type('a');
            assertCursor(result.cursor, 0, 4, Constants.CURSOR_EOF);
        });

        it('will will reach eof subsequent lines are comments until the the end of file.', function() {
            var comments = [
                [{ startIndex: 1, endIndex: null }],
                [{ startIndex: 2, endIndex: 7 }],
                [{ startIndex: 3, endIndex: null }]
            ];

            engine.loadCode('a//b\n  /*c*/  \n   //d').start(0, 200, comments.getByIndex.bind(comments));

            engine.type('a');
            var result = engine.type(Constants.CHAR_EOL);
            assertCursor(result.cursor, 2, 6, Constants.CURSOR_EOF);
        });
    });
});

describe('When starting', function() {
    let engine;

    beforeEach(function() {
        engine = new Engine();
    });

  it('should return error if code has no valid character', function() {
    var result = engine.loadCode('   \n   ').start();

    result.error.should.not.be.null;
  });

  it('should return cursor when first character is the first in code', function () {
    var result = engine.loadCode('hola').start();

    result.isComplete.should.equal(false);
    assertCursor(result.cursor, 0, 0, Constants.CURSOR_WAITING);
  });

  it('should return cursor when first character is in first line', function() {
    var result = engine.loadCode('   hola').start();
    assertCursor(result.cursor, 0, 3);

    result = engine.loadCode('  \t hola').start();
    assertCursor(result.cursor, 0, 4);
  });

  it('should return cursor when first character is in subsequent line', function() {
    var result = engine.loadCode('  \n   adios').start();
    assertCursor(result.cursor, 1, 3);

    result = engine.loadCode('\n   \nadios').start();
    assertCursor(result.cursor, 2, 0);
  });

  describe('and char count', function() {
    it('will return the end cursor at eof if min char count is met', function() {
        var result = engine.loadCode('abcdefg').start(0, 2);

        assertCursor(result.endCursor, 0, 7);
    });

    it('will return the end cursor at eol of first line if min char count is met', function() {
        var result = engine.loadCode('abc\ndef').start(0, 2);

        assertCursor(result.endCursor, 0, 3);
    });

    it('will return the end cursor at eol of middle line if min char count is met', function() {
        var result = engine.loadCode('abc\ndef\nghi').start(0, 5);

        assertCursor(result.endCursor, 1, 3);
    });

    it('will return the end cursor at eof if min count is more than code length', function() {
        var result = engine.loadCode('abc\ndef\nghi').start(0, 200);

        assertCursor(result.endCursor, 2, 3);
    });

    it('will return the end cursor at eol if min count is met at the end of the line', function() {
        var result = engine.loadCode('abc\ndef\nghi').start(0, 4);

        assertCursor(result.endCursor, 0, 3);
    });

    it('will return the end cursor at eol if min count is met at the end of the line and start row is defined', function() {
        var result = engine.loadCode('abc\ndef\nghi').start(1, 2);

        assertCursor(result.endCursor, 1, 3);
    });

    it('will return the end cursor at eof if min count is met at the end of the line and start row is defined', function() {
        var result = engine.loadCode('abc\ndef\nghi').start(1, 5);

        assertCursor(result.endCursor, 2, 3);
    });
  });

  describe('and code has comments', function() {
    it('will skip single line comment at the begining of the file', function() {
        var comments = [
            [{ startIndex: 0, endIndex: undefined }]
        ];

        var result = engine.loadCode('//comment\nfoo').start(0, 200, comments.getByIndex.bind(comments));

        assertCursor(result.cursor, 1, 0, Constants.CURSOR_WAITING);
    });

    it('will skip all single line comments at the begining of the file', function() {
        var comments = [
            [{ startIndex: 0, endIndex: undefined }],
            [],
            [{ startIndex: 0, endIndex: undefined }],
            []
        ];

        var result = engine.loadCode('    //comment1\n   \n//comment2\nfoo').start(0, 200, comments.getByIndex.bind(comments));

        assertCursor(result.cursor, 3, 0, Constants.CURSOR_WAITING);
    });

    it('will skip block comment in the first line of the file', function() {
        var comments = [
            [{ startIndex: 0, endIndex: 11 }]
        ];

        var result = engine.loadCode('/*comment*/foo').start(0, 200, comments.getByIndex.bind(comments));

        assertCursor(result.cursor, 0, 11, Constants.CURSOR_WAITING);
    });

    it('will skip all block comments in the first line of the file', function() {
        var comments = [
            [{ startIndex: 3, endIndex: 9 }, { startIndex: 12, endIndex: 18 }]
        ];

        var result = engine.loadCode('   /*c1*/   /*c2*/   foo').start(0, 200, comments.getByIndex.bind(comments));

        assertCursor(result.cursor, 0, 21, Constants.CURSOR_WAITING);
    });

    it('will skip all block comments in the begining of the file', function() {
        var comments = [
            [{ startIndex: 3, endIndex: 9 }, { startIndex: 12, endIndex: 18 }],
            [{ startIndex: 3, endIndex: 9 }, { startIndex: 12, endIndex: undefined }],
            []
        ];

        var result = engine.loadCode('   /*c1*/   /*c2*/   \n   /*c3*/   /*c4*/\n   foo').start(0, 200, comments.getByIndex.bind(comments));

        assertCursor(result.cursor, 2, 3, Constants.CURSOR_WAITING);
    });
  });
});

describe('When loading code', function () {
  it('should return error if code is not a string', function() {
    (function() { new Engine().loadCode(null); }).should.throw();
  });
});