
export const authenticated = createMixin(function() {
    if (!this.userId) {
        throw new Meteor.Error('unauthorized', 'You must be logged in to access this method.');
    };
});

function createMixin(callback) {
    var myMixin = function(methodOptions) {
        const runFunc = methodOptions.run;
        methodOptions.run = function() {
            callback.call(this, ...arguments);
            return runFunc.call(this, ...arguments);
        }

        return methodOptions;
    }

    return myMixin;
}
