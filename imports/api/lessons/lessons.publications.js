import { Meteor } from 'meteor/meteor';
import { Lessons } from './lessons.js';

if (Meteor.isServer) {
    Meteor.publish('lessons', function() {
        return Lessons.find({ userId: this.userId });
    });

    Meteor.publish('lesson', function(lessonId) {
        return Lessons.find({ userId: this.userId, _id: lessonId });
    });
}