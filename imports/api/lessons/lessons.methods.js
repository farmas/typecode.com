import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Lessons } from './lessons.js';
import * as Mixins from '../method.mixins.js';

export const removeLesson = new ValidatedMethod({
    name: 'lessons.remove',
    validate: new SimpleSchema({
        id: { type: String }
    }).validator(),
    mixins: [Mixins.authenticated],
    run({ id }) {
        Lessons.remove({ _id: id, userId: this.userId });
    }
});

export const updateLesson = new ValidatedMethod({
    name: 'lessons.update',
    validate: new SimpleSchema({
        id: { type: String },
        currentLine: { type: Number, optional: true },
        totalLines: { type: Number, optional: true }
    }).validator(),
    mixins: [Mixins.authenticated],
    run({ id, currentLine, totalLines }) {
        var newValues = {
            modifiedDate: new Date()
        }

        if (currentLine >= 0) {
            newValues.currentLine = currentLine;
        }

        if (totalLines > 0) {
            newValues.totalLines = totalLines;
        }

        return Lessons.update({ _id: id, userId: this.userId }, { $set: newValues });
    }
});

export const addLesson = new ValidatedMethod({
    name: 'lessons.add',
    validate: new SimpleSchema({
        url: { type: String },
        totalLines: { type: Number }
    }).validator(),
    mixins: [Mixins.authenticated],
    run({ url, totalLines }) {
        var newUrl = cleanUrl(url);
        var lowerCaseUrl = newUrl.toLowerCase();
        var lesson = Lessons.findOne({ cleanUrl: lowerCaseUrl, userId: this.userId });
        var result = {
            lessonExists: false,
            id: null
        };

        if (lesson) {
            result.lessonExists = true;
            result.id = lesson._id;
        } else {
            var now = new Date();

            var lastDotIndex = newUrl.lastIndexOf('.');
            var extension = lastDotIndex === -1 ? 'n/a' : newUrl.substring(lastDotIndex);

            var lastSlashIndex = newUrl.lastIndexOf('/');
            var name = lastSlashIndex === -1 ? newUrl : newUrl.substring(lastSlashIndex + 1);

            result.id = Lessons.insert({
                userId: this.userId,
                name: name,
                url: newUrl,
                cleanUrl: lowerCaseUrl,
                extension: extension,
                totalLines: totalLines || 1,
                currentLine: 0,
                createdDate: now,
                modifiedDate: now
            });
        }

        return result;
    }
});

function cleanUrl(url) {
    var queryIndex = url.lastIndexOf('?');
    if (queryIndex !== -1) {
        url = url.slice(0, queryIndex);
    }

    var hashIndex = url.lastIndexOf('#');
    if (hashIndex !== -1) {
        url = url.slice(0, hashIndex);
    }

    return url;
}