import { Meteor } from 'meteor/meteor';
import { ServiceConfiguration } from 'meteor/service-configuration';

Meteor.startup(() => {
    if (Meteor.settings.clientId && Meteor.settings.secret) {
        ServiceConfiguration.configurations.upsert({
          service: "google"
        }, {
          $set: {
            clientId: Meteor.settings.clientId,
            loginStyle: "popup",
            secret: Meteor.settings.secret
          }
        });
    }
});