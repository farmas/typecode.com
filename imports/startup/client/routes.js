import React from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { mount } from 'react-mounter';

import MainLayout from '../../ui/MainLayout'
import FreePracticePage from '../../ui/pages/FreePracticePage';
import LessonPage from '../../ui/pages/LessonPage';
import HomePage from '../../ui/pages/HomePage';
import AboutPage from '../../ui/pages/AboutPage';

var publicRoutes = FlowRouter.group();
var privateRoutes = FlowRouter.group({
  triggersEnter: [(context, redirect) => {
    if (!Meteor.userId()) {
        redirect('/');
    }
  }]
});

publicRoutes.route('/', {
  name: 'home',
  action() {
    mount(MainLayout, {
        content: <HomePage />
    });
  }
});

publicRoutes.route('/about', {
    name: 'about',
    action() {
        mount(MainLayout, {
            content: <AboutPage />
        });
    }
});

publicRoutes.route('/practice', {
  name: 'practice',
  triggersEnter: [scrollToTop],
  action() {
    mount(MainLayout, {
        content: <FreePracticePage />
    });
  }
});

privateRoutes.route('/lesson/:lessonId', {
  name: 'lesson',
  triggersEnter: [scrollToTop],
  action() {
    mount(MainLayout, {
        content: <LessonPage />
    });
  }
});

function scrollToTop() {
    window.scrollTo(0, 0);
}