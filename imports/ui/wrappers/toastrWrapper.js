import Q from 'q';

const TOASTR_OPTIONS = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

let _toastr = null;

function getToastrAsync() {
    let deferred = Q.defer();

    if (_toastr) {
        deferred.resolve(_toastr);
    } else {
        requirejs.config({
            "paths": {
              "jquery": "//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min"
            }
        });

        requirejs(['//cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.3/toastr.min.js'], function(toastr) {
            _toastr = toastr;

            toastr.options = TOASTR_OPTIONS;

            deferred.resolve(_toastr);
        });
    }

    return deferred.promise;
}

export function configure(options) {
    return getToastrAsync().done((toastr) => {
        toastr.options = options;
    });
}

export function error(message) {
    return getToastrAsync().done((toastr) => {
        toastr.error(message);
    });
}

export function warning(message) {
    return getToastrAsync().done((toastr) => {
        toastr.warning(message);
    });
}

export function success(message) {
    return getToastrAsync().done((toastr) => {
        toastr.success(message);
    });
}

export function info(message) {
    return getToastrAsync().done((toastr) => {
        toastr.info(message);
    });
}