import React from 'react';
import Q from 'q';

export default class FadingSpan extends React.Component {
    constructor(props) {
        super(props);

        this._fadeOutPromise = null;
        this._isMounted = false;

        this.state = {
            text: ''
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.hardSet) {
            this._fadeOutPromise = null;
            this.setState({ text: nextProps.text });
        } else {
            this.setText(nextProps.text);
        }
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    setText(text) {
        if (text !== this.props.text) {
            let fadeOutPromise = this._fadeOutPromise = this.fadeOut();

            fadeOutPromise.done(() => {
                if (fadeOutPromise === this._fadeOutPromise && this._isMounted) {
                    this.setState({ text: text });

                    if (!!text) {
                        $(this.refs.span).fadeIn(this.props.fadeDelay, () => {
                            this._isMounted && this.props.onTextChanged && this.props.onTextChanged();
                        });
                    }
                }
            });
        }
    }

    fadeOut() {
        let deferred = Q.defer();

        $(this.refs.span).fadeOut(this.props.fadeDelay, () => {
            deferred.resolve();
        });

        return deferred.promise;
    }

    render() {
        return (
            <span className={this.props.className} ref="span">{this.state.text}</span>
        );
    }
}
