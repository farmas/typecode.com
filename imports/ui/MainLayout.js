import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import React from 'react';
import HeaderPart from './parts/HeaderPart';

class MainLayout extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="mainLayout">
                <HeaderPart />
                {this.props.content}
            </div>
        );
    }
}

export default createContainer(() => {
  return {
    userId: Meteor.userId()
  };
}, MainLayout);