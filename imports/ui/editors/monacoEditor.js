import Q from 'q';

const LOADING_TEXT = '(Loading...)';

export default class MonacoEditor {
    constructor() {
        this._editor = null;
        this._cursorDecorations = [];
        this._errorDecorations = [];
        this._disabledRowsDecorations = [];
        this._isDisposed = false;
        this._tokensChangedListener = null;

        // hack: This function is expected deep in monaco's source.
        window.process.getuid = window.process.getuid || function() { return 0; };
    }

    loadAsync(htmlElement) {
        return this._loadMonacoAsync().then((monaco) => {
            this._editor = this._createMonacoEditor(htmlElement, monaco);
        });
    }

    dispose() {
        if (this._editor) {
            this._editor.model.dispose();
            this._editor.dispose();
        }

        if (this._tokensChangedListener) {
            this._tokensChangedListener.dispose();
        }

        this._isDisposed = true;
    }

    isDisposed() {
        return this._isDisposed;
    }

    setCode(code, url, row = 0) {
        var deferred = Q.defer();

        this._ensureLoaded();

        this._editor.model.dispose();

        let modelUri = url && window.monaco.Uri.parse(url);
        let model = window.monaco.editor.createModel(code || LOADING_TEXT, null, modelUri);

        if (code) {
            let tokensChangeListener = this._tokensChangedListener = model.addListener("modelTokensChanged", function() {
                var lineTokens = model.getLineTokens(row + 1).inflate();

                //console.log('modelTokensChanged', lineTokens);
                if (lineTokens && lineTokens.length > 1 || (lineTokens.length > 0 && !!lineTokens[0].type)) {
                    tokensChangeListener.dispose();
                    deferred.resolve();
                }
            });
        }

        this._editor.setModel(model);

        Q.delay(3000).then(() => {
            // if model hasn't been parsed in 3 seconds, automatically resolve.
            deferred.resolve();
        });

        // resolve after 1 second or until the tokens have been retrieved.
        return Q.all([Q.delay(1000), deferred.promise]);
    }

    setActiveRows(startRow, endRow, rowCount) {
        this._ensureLoaded();

        let newDecorations = [];

        if (startRow > 0) {
            newDecorations.push({
                range: new window.monaco.Range(1, 0, startRow, 1),
                options: {
                    inlineClassName: 'text-disabled',
                    isWholeLine: true
                }
            });
        }

        if (endRow < rowCount - 1) {
            newDecorations.push({
                range: new window.monaco.Range(endRow + 2, 0, rowCount, 1),
                options: {
                    inlineClassName: 'text-disabled',
                    isWholeLine: true
                }
            });
        }

        this._disabledRowsDecorations = this._editor.deltaDecorations(this._disabledRowsDecorations, newDecorations);

        this._editor.revealLinesInCenter(startRow + 1, endRow + 1);
    }

    revealPosition(cursor) {
        this._ensureLoaded();

        let line = cursor.row + 1;
        let col = cursor.col < 50 ? 1 : cursor.col + 1;

        this._editor.revealRangeInCenterIfOutsideViewport({
            startLineNumber: line,
            startColumn: col,
            endLineNumber: line,
            endColumn: col + 50
        });
    }

    setCursor(cursor, error) {
        this._ensureLoaded();

        this._cursorDecorations = this._editor.deltaDecorations(this._cursorDecorations, [{
            range: new window.monaco.Range(cursor.row + 1, cursor.col + 1, cursor.row + 1, cursor.col + 2),
            options: {
                className: cursor.type.toLowerCase().replace('_', '-')
            }
        }]);

        if (error) {
            this._errorDecorations = this._editor.deltaDecorations(this._errorDecorations, [{
                range: new window.monaco.Range(error.startRow + 1, error.startCol + 1, cursor.row + 1, cursor.col + 1),
                options: {
                    className: error.type.toLowerCase().replace('_', '-')
                }
            }]);
        } else if (this._errorDecorations.length > 0) {
            this._errorDecorations = this._editor.deltaDecorations(this._errorDecorations, []);
        }
    }

    getCommentsForLine(row) {
        this._ensureLoaded();

        var tokens = this._editor.getModel().getLineTokens(row + 1);
        var tokensArr = tokens.inflate();
        var commentTokens = _.filter(tokensArr, token => {
            return token.type.toLowerCase().indexOf('comment') >= 0;
        });

        var comments = _.map(commentTokens, function(token) {
            var tokenIndex = tokensArr.indexOf(token);
            var nextTokenIndex = tokenIndex + 1;
            var nextToken = nextTokenIndex < tokensArr.length ? tokensArr[nextTokenIndex] : null;


            return {
                startIndex: token.startIndex,
                endIndex: nextToken && nextToken.startIndex
            };
        });

        //console.log('comments', row, comments);
        return comments;
    }

    _ensureLoaded() {
        if (!this._editor) {
            throw new Error('Editor is not loaded.');
        }
    }

    _loadMonacoAsync() {
        var deferred = Q.defer();

        if (window.monaco) {
            deferred.resolve(window.monaco);
        } else {
            var originalRequire = global.require;
            global.require = requirejs;

            global.require.config({
                baseUrl: "/",
            });

            global.require(['vs/editor/editor.main'], () => {
                global.require = originalRequire;
                deferred.resolve(window.monaco);
            });
        }

        return deferred.promise;
    }

    _createMonacoEditor(htmlElement, monaco) {
        return monaco.editor.create(htmlElement, {
            value: LOADING_TEXT,
            readOnly: true,
            wrappingColumn: -1,
            glyphMargin: true,
            fontFamily: "Ubuntu Mono, monospace",
            fontSize: 16,
            contextMenu: false,
            quickSuggestions: false,
            scrollbar: {
                // Subtle shadows to the left & top. Defaults to true.
                //useShadows: false,

                // Render vertical arrows. Defaults to false.
                verticalHasArrows: true,

                // Render horizontal arrows. Defaults to false.
                horizontalHasArrows: true,

                // Render vertical scrollbar.
                vertical: 'visible',

                // Render horizontal scrollbar.
                horizontal: 'visible',

                verticalScrollbarSize: 17,
                horizontalScrollbarSize: 17,
                arrowSize: 30
            }
        });
    }
}
