import React from 'react';
import FadingSpan from '../elements/FadingSpan';

const COUNTDOWN_DELAY = 1500;
const COUNTDOWN_FADE_DELAY = 200;
const COUNTDOWN_VALUES = [
    { text: 'On Your Marks', className: 'countdown-progress' },
    { text: 'Get Set', className: 'countdown-progress' },
    { text: 'Type!', className: 'countdown-complete' }];

export default class CountDownPart extends React.Component {
    constructor(props) {
        super(props);

        this._countDownTimer = null;

        this.state = {
            countDownText: '',
            countDownClassName: '',
            countDownOnComplete: null,
            countDownHardSet: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.blockStartTime || nextProps.blockStartTime < 0) {
            this.updateCountDown({ index: -1, clearTimer: true });
        } else if (nextProps.blockStartTime !== this.props.blockStartTime) {
            // block started
            this.startCountDown();
        } else if (nextProps.isTyping !== this.props.isTyping) {
            if (nextProps.isTyping) {
                // user started to type
                this.updateCountDown({ index: COUNTDOWN_VALUES.length - 1, clearTimer: true });
            } else {
                // user stopped typing
                this.updateCountDown({ index: -1, clearTimer: true });
            }
        }
    }

    componentWillUnmount() {
        window.clearInterval(this._countDownTimer);
    }

    updateCountDown({ index, hardSet, clearTimer, onTextChanged }) {
        if (clearTimer) {
            window.clearInterval(this._countDownTimer);
        }

        if (index >= 0 && index < COUNTDOWN_VALUES.length) {
            let countDownValue = COUNTDOWN_VALUES[index];
            this.setState({
                countDownText: countDownValue.text,
                countDownClassName: countDownValue.className,
                countDownOnComplete: onTextChanged,
                countDownHardSet: !!hardSet

            });
        } else {
            this.setState({ countDownText: '', countDownOnComplete: null, countDownHardSet: true });
        }
    }

    startCountDown() {
        window.clearInterval(this._countDownTimer);
        this.updateCountDown({ index: 0, hardSet: true });

        let countIndex = 1;
        this._countDownTimer = window.setInterval(() => {
            // if this is the last count down, invoke function after the text is set.
            let onTextChanged = COUNTDOWN_VALUES.length - 1 > countIndex ? null: this.props.onCountDownComplete;
            this.updateCountDown({ index: countIndex, onTextChanged: onTextChanged, clearInterval: !!onTextChanged });

            countIndex++;
        }, COUNTDOWN_DELAY);
    }

    render() {
        return(
            <div className="countDownPart">
                <FadingSpan
                    text={this.state.countDownText}
                    className={this.state.countDownClassName}
                    hardSet={this.state.countDownHardSet}
                    onTextChanged={this.state.countDownOnComplete}
                    fadeDelay={COUNTDOWN_FADE_DELAY} />
            </div>
        );
    }
}