import React from 'react';
import classnames from 'classnames';
import ObjectAssign from 'object-assign';
import Editor from '../editors/monacoEditor';
import Engine from '../../api/typing/engine';
import * as Constants from '../../api/typing/constants';
import Stopwatch from '../../api/typing/stopwatch';
import ResultDialog from '../dialogs/ResultDialog';
import LessonCompleteDialog from '../dialogs/LessonCompleteDialog';
import CountDownPart from './CountDownPart';
import * as toastr from '../wrappers/toastrWrapper';

export default class EditorPart extends React.Component {
    constructor(props) {
        super(props);

        this.editor = new Editor();
        this.engine = new Engine();
        this.cursor = null;
        this.startCursor = null;
        this.stopwatch = new Stopwatch();

        this.editorLoadedPromise = null;
        this.keyPressHandler = null;
        this.keyUpHandler = null;

        this.state = this._getInitialState();
    }

    _getInitialState() {
        return {
            isRowOutOfRange: false,
            isTyping: false,
            hasTyped: false,
            isCodeLoaded: false,
            timer: '00:00',
            time: 0,
            blockStartTime: null,
            typeResult: Engine.getDefaultResult()
        };
    }

    resetState(isCodeLoaded = false) {
        let state = this._getInitialState();
        state.isCodeLoaded = isCodeLoaded;
        this.setState(state);
    }

    componentDidMount() {
        this.editorLoadedPromise = this.editor.loadAsync(this.refs.editorDiv);
    }

    componentWillUnmount() {
        this.stopwatch.stop();
        this.editor.dispose();
        this._detachListeners();
    }

    componentWillReceiveProps(nextProps) {
        var isNewCode = nextProps.code && nextProps.code !== this.props.code;
        var isNewConfig = nextProps.startLine !== this.props.startLine || nextProps.charCount !== this.props.charCount;
        var resetState = function() {
            if (!nextProps.code || isNewCode) {
                this.resetState();
            }
        }.bind(this);

        resetState();

        this.editorLoadedPromise.then(() => {
            if (this.editor.isDisposed()) {
                return;
            }

            resetState();

            if (!nextProps.code) {
                this.editor.setCode(null);
            } else if (isNewCode || isNewConfig) {
                this.engine.loadCode(nextProps.code);

                var formattedCode = this.engine.getFormattedCode();

                // load the formatted code into the editor and wait until it has finished tokenizing
                return this.editor.setCode(formattedCode, nextProps.url).then(() => {
                    if(!this.editor.isDisposed()) {

                        if (nextProps.startLine >= this.engine.code.length) {
                            this.setState({ isRowOutOfRange: true });
                        } else {
                            this.startBlock(nextProps.startLine - 1, nextProps.charCount);
                        }
                    }
                });
            }
        }).catch(err => {
            console.log('error', err);
        });
    }

    startTyping() {
        if (!this.state.isTyping) {
            this._attachListeners();

            var state = this.engine.getState();

            if (!this.state.hasTyped) {
                this.cursor.type = Constants.CURSOR_TYPING;
            }

            this.editor.setCursor(this.cursor, state.typeError);
            this.setState({ isTyping: true });

            this.stopwatch.start((time, formattedTime) => {
                this.setState({ time: time, timer: formattedTime });
            });
        }
    }

    stopTyping() {
        if (this.state.isTyping) {
            this._detachListeners();

            var state = this.engine.getState();
            this.cursor = state.cursor;

            var stopCursor = ObjectAssign({}, state.cursor);
            stopCursor.type = Constants.CURSOR_WAITING;

            this.editor.setCursor(stopCursor, state.typeError);
            this.stopwatch.stop();
            this.setState({ isTyping: false });
        }
    }

    onCountDownComplete() {
        $(this.refs.editorDiv).find('textarea').focus();
        this.startTyping();
    }

    restartFile() {
        this.startBlock(0);

        this.props.onFileRestarted();
    }

    startBlock(row, charCount) {
        this.setState({ blockStartTime: null });
        this.stopwatch.reset();

        charCount = charCount || this.props.charCount;

        var result = this.engine.start(
            row,
            charCount,
            this.editor.getCommentsForLine.bind(this.editor));

        if (!result.error) {
            this.cursor = result.cursor;
            this.startCursor = ObjectAssign({}, this.cursor);
            this.editor.setCursor(result.cursor);
            this.editor.setActiveRows(result.cursor.row, result.endCursor.row, result.rowCount);

            this.resetState(true);
            this.setState({ blockStartTime: new Date().getTime(), typeResult: result });
        } else {
            this.resetState(false);
            toastr.error(`Failed to start at line ${row + 1}: ${result.error.message}`);
        }
    }

    restartBlock() {
        if (this.startCursor) {
            this.startBlock(this.startCursor.row);
        }
    }

    finishBlock(typeResult) {
        this._detachListeners();

        this.setState({ typeResult: typeResult });

        this.props.onBlockFinished(typeResult);
        this.stopwatch.stop();
    }

    nextBlock() {
        var endCursor = this.engine.endCursor;

        if (endCursor && endCursor.row < this.engine.code.length - 1) {
            this.startBlock(endCursor.row + 1);
        }
    }

    skipBlock() {
        var state = this.engine.getState();
        this.finishBlock(state);
        this.nextBlock();
    }

    shouldCaptureKey(event) {
        let tagName = event.target.tagName.toLowerCase();
        return tagName === 'body' || tagName === 'textarea';
    }

    onKeyPress(event) {
        var key = String.fromCharCode(event.keyCode);

        if (this.shouldCaptureKey(event) && key && key !== '\r' && key !== '\n') {
            event.preventDefault();

            //console.log('keypress', event.keyCode, key);
            var result = this.engine.type(key);
            this.editor.setCursor(result.cursor, result.typeError);
            this.editor.revealPosition(result.cursor);

            if (!this.state.hasTyped) {
                this.setState({ hasTyped: true });
            }

            if (result.isComplete) {
                this.finishBlock(result);
            }
        }
    }

    onKeyUp(event) {
        var result;

        if (!this.shouldCaptureKey(event)) {
            return;
        } else if (event.keyCode === 8) {
            result = this.engine.backspace();
        } else if (event.keyCode === 13) {
            result = this.engine.type(Constants.CHAR_EOL);
        }

        if (result) {
            event.preventDefault();

            //console.log('keyup', event.keyCode);
            this.editor.setCursor(result.cursor, result.typeError);
            this.editor.revealPosition(result.cursor);

            if (result.isComplete) {
                this.finishBlock(result);
            }
        }
    }

    getControlButtons() {
        let isLoadedAndIncomplete = this.state.isCodeLoaded && !this.state.typeResult.isComplete;
        let isStartButtonActive = isLoadedAndIncomplete && !this.state.isTyping;
        let isStopButtonActive = isLoadedAndIncomplete && this.state.isTyping;
        let isRestartButtonActive = this.state.isCodeLoaded;
        let controlClassNames = ['control', 'fa', 'fa-2x'];

        let startClassNames = classnames('fa-play', controlClassNames, { active: isStartButtonActive });
        let stopClassNames = classnames('fa-pause', controlClassNames, { active: isStopButtonActive });
        let restartClassNames = classnames('fa-repeat', controlClassNames, { active: isRestartButtonActive });

        return ([
            <i key="startButton" className={startClassNames} onClick={isStartButtonActive ? this.startTyping.bind(this) : null }></i>,
            <i key="stopButton" className={stopClassNames} onClick={isStopButtonActive ? this.stopTyping.bind(this) : null }></i>,
            <i key="restartButton" className={restartClassNames} onClick={isRestartButtonActive ? this.restartBlock.bind(this) : null }></i>,
        ]);
    }

    render() {
        let editorClassNames = classnames('editor-div', { 'text-disabled': !this.state.isCodeLoaded });

        return (
            <div className="editorPart">
                <div className="controls">
                    <div className="buttons">
                        {this.getControlButtons()}
                    </div>

                    <div className="timer">
                        {this.state.timer}
                    </div>

                    <div className="countdown">
                        <CountDownPart
                            isTyping={this.state.isTyping}
                            blockStartTime={this.state.blockStartTime}
                            onCountDownComplete={this.onCountDownComplete.bind(this)} />
                    </div>
                </div>

                <div className={editorClassNames} ref="editorDiv"></div>

                <div className="bottom-controls">
                    { this.state.isCodeLoaded && (this.state.typeResult.stats.lastBlockLine < this.state.typeResult.stats.totalLines) &&
                        <button className="c-button c-button--info" onClick={this.skipBlock.bind(this)}>
                            Skip Block&nbsp;&nbsp;
                            <i className="fa fa-fast-forward"></i>
                        </button>
                    }
                </div>

                <ResultDialog
                    show={this.state.typeResult.isComplete}
                    showLessonsButton={this.props.showLessonsButton}
                    result={this.state.typeResult}
                    timer={this.state.timer}
                    time={this.state.time}
                    restart={this.restartBlock.bind(this)}
                    next={this.nextBlock.bind(this)}
                    end={this.restartFile.bind(this)} />

                <LessonCompleteDialog
                    show={this.state.isRowOutOfRange}
                    end={this.restartFile.bind(this)}
                    showLessonsButton={this.props.showLessonsButton} />
            </div>
        );
    }

    _attachListeners() {
        if (!this.keyPressHandler) {
            $('body').on('keypress', this.keyPressHandler = this.onKeyPress.bind(this));
            $('body').on('keyup', this.keyUpHandler = this.onKeyUp.bind(this));
        }
    }

    _detachListeners() {
        $('body').off('keypress', this.keyPressHandler);
        $('body').off('keyup', this.keyUpHandler);

        this.keyPressHandler = null;
        this.keyUpHandler = null;
    }
}

EditorPart.defaultProps = {
    onBlockFinished: () => {},
    onFileRestarted: () => {}
};
