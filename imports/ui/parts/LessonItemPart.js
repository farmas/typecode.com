import React from 'react'
import { FlowRouter } from 'meteor/kadira:flow-router';
import classnames from 'classnames';
import * as toastr from '../wrappers/toastrWrapper';
import * as LessonMethods from '../../api/lessons/lessons.methods.js';

export default class LessonItemPart extends React.Component {
    removeLesson(event) {
        event.stopPropagation();

        LessonMethods.removeLesson.call({ id: this.props.lesson._id }, (error, result) => {
            if (error) {
                let message = 'Error deleting lesson, please try again.'
                console.log(message, err);
                toastr.error(message);
            } else {
                console.log('Lesson deleted.');
            }
        });
    }

    loadLesson() {
        FlowRouter.go('lesson', { lessonId: this.props.lesson._id });
    }

    render() {
        let currentLine = this.props.lesson.currentLine;
        let totalLines = this.props.lesson.totalLines;
        let percentComplete = Math.round((currentLine / totalLines) * 100);
        let progressClassNames = classnames('c-progress__bar', { 'c-progress__bar--info': percentComplete === 100 });
        let lastAccessed = moment(this.props.lesson.modifiedDate).startOf('minute').fromNow();

        return (
            <div className="lessonItemPart" onClick={this.loadLesson.bind(this)}>
                <div className="c-card u-high">
                  <div className="c-card__item c-card__item--divider lesson-header">
                    {this.props.lesson.name}
                    <div className="removeLesson" onClick={this.removeLesson.bind(this)}>
                        <i className="fa fa-trash-o" aria-hidden="true"></i>
                    </div>
                    <span className="last-accessed">( {lastAccessed} )</span>
                  </div>
                  <div className="c-card__item">
                    <div className="c-paragraph">
                        {this.props.lesson.url}
                    </div>
                    <div className="c-paragraph">
                        <div className="c-progress u-medium">
                            <div className={progressClassNames} style={{width: percentComplete + '%'}} >
                                <span className="progress-text">Current line {currentLine} of {totalLines} ({percentComplete}%)</span>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        );
    }
}