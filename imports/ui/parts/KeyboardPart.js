import React from 'react';
import Key from './KeyPart';
import * as Constants from '../../api/typing/constants';

export default class KeyboardPart extends React.Component {
    constructor(props) {
        super(props);

        this._blockSize = 1;
    }

    buildKey({ value1, value2, showValue2, id, classes, text1 }) {
        return (
            <Key
                value1={value1}
                text1={text1}
                value2={value2}
                showValue2={showValue2}
                id={id}
                classes={classes}
                heatMap={this.props.heatMap}
                blockSize={this._blockSize} />
        );
    }

    componentWillReceiveProps(nextProps) {
        let maxErrors = _.max(Object.keys(nextProps.heatMap), keyName => nextProps.heatMap[keyName].count) || 0;
        this._blockSize = Math.floor(maxErrors / 3) || 1;
    }

    render() {
        return (
            <div className="keyboardPart">
                <ul className="cf" id="numbers">
                    { this.buildKey({ value1: '`', value2: '~', showValue2: true}) }
                    { this.buildKey({ value1: '1', value2: '!', showValue2: true}) }
                    { this.buildKey({ value1: '2', value2: '@', showValue2: true}) }
                    { this.buildKey({ value1: '3', value2: '#', showValue2: true}) }
                    { this.buildKey({ value1: '4', value2: '$', showValue2: true}) }
                    { this.buildKey({ value1: '5', value2: '%', showValue2: true}) }
                    { this.buildKey({ value1: '6', value2: '^', showValue2: true}) }
                    { this.buildKey({ value1: '7', value2: '&', showValue2: true}) }
                    { this.buildKey({ value1: '8', value2: '*', showValue2: true}) }
                    { this.buildKey({ value1: '9', value2: '(', showValue2: true}) }
                    { this.buildKey({ value1: '0', value2: ')', showValue2: true}) }
                    { this.buildKey({ value1: '-', value2: '_', showValue2: true}) }
                    { this.buildKey({ value1: '=', value2: '+', showValue2: true}) }
                    { this.buildKey({ value1: 'delete', id: 'delete' }) }
                </ul>
                <ul className="cf" id="qwerty">
                    <li><a href="#" className="key c9 fsa-no-hover" id="tab"><span>tab</span></a></li>
                    { this.buildKey({ value1: 'q', value2: 'Q' }) }
                    { this.buildKey({ value1: 'w', value2: 'W' }) }
                    { this.buildKey({ value1: 'e', value2: 'E' }) }
                    { this.buildKey({ value1: 'r', value2: 'R' }) }
                    { this.buildKey({ value1: 't', value2: 'T' }) }
                    { this.buildKey({ value1: 'y', value2: 'Y' }) }
                    { this.buildKey({ value1: 'u', value2: 'U' }) }
                    { this.buildKey({ value1: 'i', value2: 'I' }) }
                    { this.buildKey({ value1: 'o', value2: 'O' }) }
                    { this.buildKey({ value1: 'p', value2: 'P' }) }
                    { this.buildKey({ value1: '[', value2: '{', showValue2: true, classes: 'alt'}) }
                    { this.buildKey({ value1: ']', value2: '}', showValue2: true, classes: 'alt'}) }
                    { this.buildKey({ value1: '\\', value2: '|', showValue2: true, classes: 'alt'}) }
                </ul>
                <ul className="cf" id="asdfg">
                    <li><a href="#" className="key c20 alt fsa-no-hover" id="caps"><b></b><span>caps lock</span></a></li>
                    { this.buildKey({ value1: 'a', value2: 'A' }) }
                    { this.buildKey({ value1: 's', value2: 'S' }) }
                    { this.buildKey({ value1: 'd', value2: 'D' }) }
                    { this.buildKey({ value1: 'f', value2: 'F' }) }
                    { this.buildKey({ value1: 'g', value2: 'G' }) }
                    { this.buildKey({ value1: 'h', value2: 'H' }) }
                    { this.buildKey({ value1: 'j', value2: 'J' }) }
                    { this.buildKey({ value1: 'k', value2: 'K' }) }
                    { this.buildKey({ value1: 'l', value2: 'L' }) }
                    { this.buildKey({ value1: ';', value2: ':', showValue2: true, classes: 'alt' }) }
                    { this.buildKey({ value1: '\'', value2: '"', showValue2: true, classes: 'alt' }) }
                    { this.buildKey({ text1: 'return', value1: Constants.CHAR_EOL, id: 'enter', classes: 'alt'}) }
                </ul>
                <ul className="cf" id="zxcvb">
                    <li><a href="#" className="key c16 shiftleft fsa-no-hover"><span>Shift</span></a></li>
                    { this.buildKey({ value1: 'z', value2: 'Z' }) }
                    { this.buildKey({ value1: 'x', value2: 'X' }) }
                    { this.buildKey({ value1: 'c', value2: 'C' }) }
                    { this.buildKey({ value1: 'v', value2: 'V' }) }
                    { this.buildKey({ value1: 'b', value2: 'B' }) }
                    { this.buildKey({ value1: 'n', value2: 'N' }) }
                    { this.buildKey({ value1: 'm', value2: 'M' }) }
                    { this.buildKey({ value1: ',', value2: '<', showValue2: true, classes: 'alt' }) }
                    { this.buildKey({ value1: '.', value2: '>', showValue2: true, classes: 'alt' }) }
                    { this.buildKey({ value1: '/', value2: '?', showValue2: true, classes: 'alt' }) }
                    <li><a href="#" className="key c16 shiftright fsa-no-hover"><span>Shift</span></a></li>
                </ul>
                <ul className="cf" id="bottomrow">
                    <li><a href="#" className="key fsa-hidden" id="fn"><span>fn</span></a></li>
                    <li><a href="#" className="key c17 fsa-hidden" id="control"><span>control</span></a></li>
                    <li><a href="#" className="key option fsa-hidden" id="optionleft"><span>option</span></a></li>
                    <li><a href="#" className="key command fsa-hidden" id="commandleft"><span>command</span></a></li>
                    { this.buildKey({ value1: ' ', id:'spacebar' }) }
                    <li><a href="#" className="key command fsa-hidden" id="commandright"><span>command</span></a></li>
                    <li><a href="#" className="key option fsa-hidden" id="optionright"><span>option</span></a></li>
                </ul>
            </div>
        );
    }
}
