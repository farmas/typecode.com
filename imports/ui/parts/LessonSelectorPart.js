import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Lessons } from '../../api/lessons/lessons.js';
import LessonItemPart from '../parts/LessonItemPart';

class LessonSelectorPart extends React.Component {
    render() {
        var lessons = this.props.lessons.map((lesson, index) => {
            return <LessonItemPart key={index} lesson={lesson} />
        });

        return (
            <div className="lessonSelectorPart">
                <div className="fsa-centered">
                    <p>
                        Continue from an existing lesson:
                    </p>

                    { this.props.loading &&
                        <p>
                            <i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                        </p>
                    }

                    { !this.props.loading && lessons.length === 0 &&
                        <p className="empty-lessons">
                            No lessons created.
                        </p>
                    }
                </div>

                {lessons}
            </div>
        );
    }
}

export default createContainer(() => {
    const lessonsHandle = Meteor.subscribe('lessons');
    const loading = !lessonsHandle.ready();

    return {
        loading: loading,
        lessons: Lessons.find({}, { sort: { modifiedDate: -1 }}).fetch()
    };
}, LessonSelectorPart);
