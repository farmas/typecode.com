import React from 'react';
import AccountsUI from '../wrappers/AccountsWrapper';

export default class HeaderPart extends React.Component {
    render() {
        return (
            <div className="headerPart">
                <div className="c-nav c-nav--inline">
                    <a href="/">
                        <span className="c-nav__content u-window-box--none">
                            <img className="logo" src="/images/keyboard-medium-white.png" />
                        </span>
                    </a>
                    <a className="c-nav__content fsa-brand" href="/">Type Code</a>
                    <a className="c-nav__item" href="/about">About</a>
                    <a className="c-nav__item" href="https://bitbucket.org/farmas/typecode.com" target="_blank">Source</a>
                    <a className="c-nav__item" href="http://blog.federicosilva.net/search/label/type-code.com" target="_blank">Blog</a>

                    <div className="c-nav__content c-nav__item--right">
                        <AccountsUI />
                    </div>
                </div>
            </div>
        );
    }
}