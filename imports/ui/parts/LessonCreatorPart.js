import React from 'react';
import { FlowRouter } from 'meteor/kadira:flow-router';
import FileSelectorPart from './FileSelectorPart';
import LessonExistsDialog from '../dialogs/LessonExistsDialog';
import * as toastr from '../wrappers/toastrWrapper';
import * as LessonMethods from '../../api/lessons/lessons.methods.js';
import * as Constants from '../../api/constants';

export default class LessonCreatorPart extends React.Component {
    constructor(props) {
        super(props);

        this._isMounted = false;

        this.state = {
            lessonExists: false,
            lessonId: null,
            loadingCode: false,
            code: null,
            startLine: 1,
            charCount: Constants.LessonCharCount,
            url: 'https://bitbucket.org/farmas/typecode.com/raw/master/imports/api/typing/engine.js'
        };
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleInputChange(event) {
        var newState = {};
        var propName = event.target.name;
        var propValue = event.target.value;

        newState[propName] = propValue;

        this.setState(newState);
    }

    onLessonExistsCancel() {
        this.setState({ lessonExists: false, lessonId: null });
    }

    loadCode() {
        let logError = (message, err) => {
            console.log(message, err);
            toastr.error(message);
            this.setState({ loadingCode: false });
        }

        console.log('[LessonCreatorPart] Downloading code.');

        this.setState({ code: null, loadingCode: true });

        $.ajax({
            url: this.state.url,
            dataType: 'text'
        }).fail(err => {
            logError('Error retrieving file. Please try again.', err);
        }).done((data) => {
            console.log('Adding lesson.');

            let totalLines = data && data.split('\n').length;

            this._isMounted && LessonMethods.addLesson.call({ url: this.state.url, totalLines: totalLines || 1 }, (error, result) => {
                if (error) {
                    logError('Error creating lesson, please try again.', error);
                } else if (result.lessonExists) {
                    console.log('Lesson already exists.', result.id);
                    this._isMounted && this.setState({ lessonExists: true, lessonId: result.id });
                } else {
                    console.log('Lesson created. ', result.id);
                    this._isMounted && FlowRouter.go('lesson', { lessonId: result.id });
                }
            });
        });
    }

    render() {
        return (
            <div className="lessonCreatorPart">
                <p className="fsa-centered">
                    Create a New Lesson:
                </p>

                <FileSelectorPart
                    url={this.state.url}
                    loadButtonText="Create"
                    enableOptions={false}
                    startLineText={this.state.startLine.toString()}
                    charCountText={this.state.charCount.toString()}
                    handleInputChange={this.handleInputChange.bind(this)}
                    loadCode={this.loadCode.bind(this)} />

                <LessonExistsDialog
                    show={this.state.lessonExists}
                    url={this.state.url}
                    lessonId={this.state.lessonId}
                    onCancel={this.onLessonExistsCancel.bind(this)} />
            </div>
        );
    }
}
