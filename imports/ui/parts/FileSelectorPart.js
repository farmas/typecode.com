import React from 'react';

export default class FileSelectorPart extends React.Component {
    onFocus(event) {
        $(event.target).select();
    }

    render() {
        return (
            <div className="fileSelectorPart">
                <div className="o-form-element">
                    <div className="c-input-group">
                        <label className="fsa-span" htmlFor="inputCode">Code File:</label>
                        <div className="o-field">
                            <input
                                id="inputCode"
                                name="url"
                                className="c-field"
                                type="text"
                                placeholder="File Url"
                                value={this.props.url}
                                onFocus={this.onFocus.bind(this)}
                                onChange={this.props.handleInputChange} />
                        </div>
                        <button className="c-button c-button--brand" onClick={this.props.loadCode}>{this.props.loadButtonText}</button>
                    </div>
                    {this.props.enableOptions &&
                    <div className="file-options">
                        <div className="file-option">
                            <div className="c-input-group">
                                <label className="fsa-span" htmlFor="inputStartRow">Start Line:</label>
                                <div className="o-field">
                                    <input
                                        id="inputStartRow"
                                        name="startLineText"
                                        className="c-field"
                                        type="text"
                                        placeholder="Start Line"
                                        value={this.props.startLineText}
                                        onFocus={this.onFocus.bind(this)}
                                        onChange={this.props.handleInputChange} />
                                </div>
                            </div>
                        </div>
                        <div className="file-option">
                            <div className="c-input-group">
                                <label className="fsa-span" htmlFor="inputKeyCount">Key Count:</label>
                                <div className="o-field">
                                    <input
                                        id="inputKeyCount"
                                        name="charCountText"
                                        className="c-field"
                                        type="text"
                                        placeholder="Min Char Count"
                                        value={this.props.charCountText}
                                        onFocus={this.onFocus.bind(this)}
                                        onChange={this.props.handleInputChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                    }
                </div>
            </div>
        );
    }
}

FileSelectorPart.defaultProps = {
    enableOptions: true,
    loadButtonText: 'Load'
};
