import React from 'react';
import classnames from 'classnames';
import * as Constants from '../../api/typing/constants';

export default class KeyboardPart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showTooltip: false
        }
    }

    onMouseEnter(event) {
        this.setState({ showTooltip: true });
    }

    onMouseLeave(event) {
        this.setState({ showTooltip: false });
    }

    _getErrorInfo(propName) {
        return this.props.heatMap[propName] || { count: 0 };
    }

    _buildTooltipKeyFromMap(errorInfo) {
        let typedChars = errorInfo.typedChars;
        let maxVal = 0;
        let errorKey = 'N/A';

        _.each(Object.keys(typedChars), (key) => {
            let val = typedChars[key];
            if (val > maxVal) {
                maxVal = val;
                errorKey = key;
            }
        });

        return this._buildTooltipKey(errorKey);
    }

    _buildTooltipKey(text) {
        if (text === ' ') {
            text = 'SPC';
        } else if (text === Constants.CHAR_EOL) {
            text = 'RET';
        }

        return (
            <div className="fixed-size-square">
                <span>{text}</span>
            </div>
        );
    }

    _buildErrorMessage(errorInfo, keyValue) {
        return (
            <div className="o-grid o-grid--no-gutter">
              <div className="o-grid__cell">
                { keyValue && this._buildTooltipKey(keyValue)}
                <span className="fsa-error-description"> <i className="fa fa-arrow-right"></i> {errorInfo.count} errors. Instead typed: </span>
                {this._buildTooltipKeyFromMap(errorInfo)}
              </div>
            </div>
        );
    }

    _buildTooltipContent(val1Error, val2Error) {
        if (this.props.value2) {
            return (
                <div>
                    { val1Error.count > 0 && this._buildErrorMessage(val1Error, this.props.value1) }

                    { val1Error.count > 0 && val2Error.count > 0 && <br /> }

                    { val2Error.count > 0 && this._buildErrorMessage(val2Error, this.props.value2) }
                </div>
            );
        } else {
            return this._buildErrorMessage(val1Error);
        }
    }

    render() {
        let val1Error = this._getErrorInfo(this.props.value1);
        let val2Error = this._getErrorInfo(this.props.value2);
        let errorCount = val1Error.count + val2Error.count;

        let val1BlockNumber = Math.floor(val1Error.count / this.props.blockSize);
        let val2BlockNumber = Math.floor(val2Error.count / this.props.blockSize);
        let keyBlockNumber = Math.max(val1BlockNumber, val2BlockNumber);

        let keyClassNames = classnames('key', this.props.classes, {
            'small-error': keyBlockNumber === 1,
            'medium-error': keyBlockNumber === 2,
            'large-error': keyBlockNumber >= 3
        });

        return (
            <li className="fsa-key" onMouseEnter={this.onMouseEnter.bind(this)} onMouseLeave={this.onMouseLeave.bind(this)}>
                { this.state.showTooltip && errorCount > 0 &&
                    <div className="fsa-bubble c-bubble c-bubble--bottom">
                        {this._buildTooltipContent(val1Error, val2Error)}
                    </div>
                }
                <a href="#" className={keyClassNames} id={this.props.id}>
                    {this.props.showValue2 &&
                        <b>{this.props.value2}</b>
                    }
                    <span>{this.props.text1 || this.props.value1}</span>
                </a>
            </li>
        );
    }
}
