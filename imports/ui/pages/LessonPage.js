import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Lessons } from '../../api/lessons/lessons.js';
import EditorPart from '../parts/EditorPart';
import * as toastr from '../wrappers/toastrWrapper';
import * as LessonMethods from '../../api/lessons/lessons.methods.js';
import * as Constants from '../../api/constants';

class LessonPage extends React.Component {
    constructor(props) {
        super(props);

        this._isMounted = false;
        this._isLessonLoaded = false;

        let charCount = FlowRouter.getQueryParam('charcount') || Constants.LessonCharCount;

        this.state = {
            code: null,
            url: null,
            startLine: 1,
            charCount: +charCount
        };
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    loadCode() {
        if (!this.props.lesson) {
            return;
        }

        var url = this.props.lesson.url;
        console.log('[LessonPage] Loading url', url);

        $.ajax({
            url: url,
            dataType: 'text'
        }).fail(err => {
            console.log('Error retrieving file', err);
            toastr.error("Error retrieving file. Please try again.");
        }).done((data) => {
            if (this._isMounted && this.props.lesson) {
                this.setState({
                    code: data,
                    url: this.props.lesson.url,
                    startLine: this.props.lesson.currentLine || 1
                });

                this.updateLesson();
            }
        });
    }

    onBlockFinished(typeResult) {
        this.updateLesson(typeResult.stats.lastBlockLine, typeResult.stats.totalLines);
    }

    onFileRestarted() {
        this.updateLesson(0);
    }

    updateLesson(currentLine, totalLines) {
        if (!this.props.lesson) {
            return;
        }

        let callParams = {
            id: this.props.lesson._id,
            currentLine: currentLine,
            totalLines: totalLines
        };

        LessonMethods.updateLesson.call(callParams, (error, result) => {
            if (error) {
                let message = 'Lesson progress could not be saved.';
                console.log(message, error);
                toastr.info(message);
            } else {
                console.log('Lesson updated. ' + result);
            }
        });
    }

    render() {
        if (this.props.lesson && !this._isLessonLoaded) {
            this._isLessonLoaded = true;

            this.loadCode();
        }

        return (
            <div className="lessonPage">
                <div className="o-container o-container--xlarge">
                    <EditorPart
                        code={this.state.code}
                        url={this.state.url}
                        startLine={this.state.startLine}
                        charCount={this.state.charCount}
                        showLessonsButton={true}
                        onBlockFinished={this.onBlockFinished.bind(this)}
                        onFileRestarted={this.onFileRestarted.bind(this)} />
                </div>
            </div>
        );
    }
}

export default createContainer(() => {
    if (!Meteor.userId()) {
        FlowRouter.go('home');
    }

    var lessonId = FlowRouter.getParam('lessonId');

    const lessonHandle = Meteor.subscribe('lesson', lessonId);
    const loading = !lessonHandle.ready();

    return {
        loading: loading,
        lesson: Lessons.findOne({ _id: lessonId })
    };
}, LessonPage);