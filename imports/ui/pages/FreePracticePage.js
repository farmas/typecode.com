import React from 'react';
import EditorPart from '../parts/EditorPart';
import FileSelectorPart from '../parts/FileSelectorPart';
import * as toastr from '../wrappers/toastrWrapper';
import * as Constants from '../../api/constants';

export default class FreePracticePage extends React.Component {
    constructor(props) {
        super(props);

        this._currentUrl = null;
        this._isMounted = false;

        let charCount = FlowRouter.getQueryParam('charcount') || Constants.PracticeCharCount;
        let startLine = FlowRouter.getQueryParam('line') || 1;

        this.state = {
            loadingCode: false,
            code: null,
            startLineText: startLine.toString(),
            startLine: +startLine,
            charCountText: charCount.toString(),
            charCount: +charCount,
            url: 'https://bitbucket.org/farmas/typecode.com/raw/master/imports/api/typing/engine.js'
        };
    }

    componentDidMount() {
        this._isMounted = true;
        this.loadCode();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleInputChange(event) {
        var newState = {};
        var propName = event.target.name;
        var propValue = event.target.value;

        newState[propName] = propValue;

        this.setState(newState);
    }

    loadCode() {
        if (this.state.loadingCode) {
            return;
        }

        // validate the inputs
        let charCount = +this.state.charCountText;
        let startLine = +this.state.startLineText;

        let isPositiveNumber = val => typeof val === 'number' && val > 0;

        if (!isPositiveNumber(charCount) || !isPositiveNumber(startLine)) {
            toastr.error("Failed to load file: Invalid arguments.");
        } else {
            if (this._currentUrl === this.state.url) {
                this.setState({ charCount: charCount, startLine: startLine });
            } else {
                console.log('[FreePracticePage] Loading url', this.state.url);
                this.setState({ code: null, loadingCode: true });

                $.ajax({
                    url: this.state.url,
                    dataType: 'text'
                }).done((data) => {
                    this._currentUrl = this.state.url;
                    this._isMounted && this.setState({ code: data, charCount: charCount, startLine: startLine });
                }).fail(err => {
                    console.log('Error retrieving file', err);
                    toastr.error("Error retrieving file. Please try again.");
                }).always(() => {
                    this._isMounted && this.setState({ loadingCode: false });
                });
            }
        }
    }

    render() {
        return (
            <div className="freePracticePage">
                <div className="o-container o-container--large">
                    <FileSelectorPart
                        url={this.state.url}
                        startLineText={this.state.startLineText}
                        charCountText={this.state.charCountText}
                        handleInputChange={this.handleInputChange.bind(this)}
                        loadCode={this.loadCode.bind(this)} />
                </div>

                <div className="o-container o-container--xlarge">
                    <EditorPart
                        code={this.state.code}
                        url={this.state.url}
                        startLine={this.state.startLine}
                        charCount={this.state.charCount} />
                </div>
            </div>
        );
    }
}
