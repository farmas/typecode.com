import React from 'react';

export default class AboutPage extends React.Component {
    render() {
        return (
            <div className="aboutPage o-container o-container--medium">
                <h2 className="c-heading">About</h2>
                <p className="c-paragraph">
                    This is a personal web site written by <a href="http://federicosilva.net/">Federico Silva Armas</a> to help practice touch typing for developers.
                    It was inspired by <a href="https://typing.io/">typing.io</a> and <a href="http://typingclub.com/">typingclub.com</a>.
                </p>
                <p className="c-paragraph">
                    It is not meant for production use and the database may be cleared at any time.
                    Source code is available at <a href="https://bitbucket.org/farmas/typecode.com">Bitbucket</a> and there is a <a href="http://blog.federicosilva.net/search/label/type-code.com">blog post series</a> about how this app was built.
                </p>

                <h2 className="c-heading">Technologies used</h2>
                <p className="c-paragraph">
                    <ul className="c-list">
                        <li className="c-list__item"><a href="https://nodejs.org">Node</a></li>
                        <li className="c-list__item"><a href="https://www.meteor.com/">Meteor</a></li>
                        <li className="c-list__item"><a href="https://facebook.github.io/react/">React</a></li>
                        <li className="c-list__item"><a href="https://github.com/Microsoft/monaco-editor">Monaco</a></li>
                        <li className="c-list__item"><a href="https://www.mongodb.com/">MongoDB</a></li>
                        <li className="c-list__item"><a href="http://blazecss.com/">BlazeCSS</a></li>
                    </ul>
                </p>
            </div>
        );
    }
}
