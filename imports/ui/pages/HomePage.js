import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import React from 'react';
import LessonCreatorPart from '../parts/LessonCreatorPart';
import LessonSelectorPart from '../parts/LessonSelectorPart';

class HomePage extends React.Component {
    render() {
        let separator = (
            <div className="separator">
                <div className="line"></div>
                <div className="text">Or</div>
            </div>
        );

        return (
            <div className="homePage">
                <div className="o-container o-container--small quick-practice fsa-centered">
                    <a className="c-button c-button--block c-button--success" href="/Practice">Start Quick Practice</a>
                </div>

                <div className="o-container o-container--large">
                    {separator}

                    {!this.props.userId &&
                        <p className="fsa-centered">
                            Login to Access your Lessons.
                        </p>
                    }

                    {this.props.userId &&
                        <div>
                            <LessonCreatorPart />
                            {separator}
                            <LessonSelectorPart />
                        </div>
                    }
                </div>
            </div>
        );
    }
}

export default createContainer(() => {
  return {
    userId: Meteor.userId()
  };
}, HomePage);
