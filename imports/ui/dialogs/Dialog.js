import React from 'react';

export default class Dialog extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.show !== this.props.show;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.show !== this.props.show) {
            $(this.refs.overlayDiv).toggleClass('c-overlay', nextProps.show);

            if (nextProps.show) {
                $(this.refs.modalDiv).fadeIn("slow");
            } else {
                $(this.refs.modalDiv).fadeOut("fast");
            }
        }
    }

    shouldComponentUpdate(nextProps) {
        return !!nextProps.show;
    }

    getContent() {
        return 'Default Content'
    }

    render() {
        return (
            <div className="dialog">
                <div ref="overlayDiv"></div>
                <div className="o-modal fsa-modal" ref="modalDiv">
                    {this.getContent()}
                </div>
            </div>
        );
    }
}
