import React from 'react';
import Dialog from './Dialog';
import { FlowRouter } from 'meteor/kadira:flow-router';

export default class LessonCompleteDialog extends Dialog {
    onLessonsClick() {
        FlowRouter.go('home');
    }

    getContent() {
        return (
            <div className="lessonCompleteDialog">
                <div className="c-card">
                    <div className="c-card__body fsa-centered">
                        <h3 className="c-heading">End of file has been reached.</h3>
                    </div>
                    <footer className="c-card__footer">
                          {this.props.showLessonsButton &&
                          <button className="c-button c-button--brand fsa-button" onClick={this.onLessonsClick.bind(this)}>
                            <i className="fa fa-long-arrow-left"></i>
                            &nbsp;&nbsp; Back to Lessons
                          </button>
                          }

                          <button className="c-button c-button--info fsa-button" onClick={this.props.end}>
                            Restart File&nbsp;&nbsp;
                            <i className="fa fa-level-up"></i>
                           </button>
                    </footer>
                  </div>
            </div>
        );
    }
}