import React from 'react';
import Dialog from './Dialog';
import { FlowRouter } from 'meteor/kadira:flow-router';
import KeyboardPart from '../parts/KeyboardPart';

export default class ResultDialog extends Dialog {
    onLessonsClick() {
        FlowRouter.go('home');
    }


    getContent() {
        let result = this.props.result;
        let stats = result.stats;
        let wpm = 0;
        let overhead = 0;
        let badKeyStrokes = stats.totalKeysTyped - stats.totalChars;
        let seconds = Math.round(this.props.time / 1000);
        let words = stats.totalChars / 5;

        if (stats.totalChars > 0) {
            overhead = Math.round((badKeyStrokes / stats.totalChars) * 100);
        }

        if (seconds > 0) {
            wpm = Math.round((words * 60) / seconds);
        }

        let nextButton = (<button className="c-button c-button--info fsa-button" onClick={this.props.next}>
                            Next Block&nbsp;&nbsp;
                            <i className="fa fa-fast-forward"></i>
                           </button>);

        let endButton = (<button className="c-button c-button--info fsa-button" onClick={this.props.end}>
                            Restart File&nbsp;&nbsp;
                            <i className="fa fa-level-up"></i>
                           </button>);

        return (
            <div className="resultDialog">
                <div className="c-card">
                    <header className="c-card__header fsa-header">
                        <h2 className="c-heading">Results</h2>
                    </header>
                    <div className="c-card__body">
                        <div className="o-container o-container--small">
                          <div className="o-grid fsa-grid">
                              <div className="o-grid__cell fsa-grid-label">Total Characters:</div>
                              <div className="o-grid__cell fsa-cell-success">{stats.totalChars}</div>
                          </div>
                          <div className="o-grid fsa-grid">
                              <div className="o-grid__cell fsa-grid-label">Typed Characters:</div>
                              <div className="o-grid__cell fsa-cell-info">{stats.totalKeysTyped}</div>
                          </div>
                          <div className="o-grid fsa-grid">
                              <div className="o-grid__cell fsa-grid-label">Uproductive Key Strokes:</div>
                              <div className="o-grid__cell fsa-cell-error">{badKeyStrokes} ({overhead}%)</div>
                          </div>
                          <div className="o-grid fsa-grid">
                              <div className="o-grid__cell fsa-grid-label">Elapsed Time:</div>
                              <div className="o-grid__cell fsa-cell-info">{this.props.timer}</div>
                          </div>
                          <div className="o-grid fsa-grid">
                              <div className="o-grid__cell fsa-grid-label">Words per Minute:</div>
                              <div className="o-grid__cell fsa-cell-info">{wpm}</div>
                          </div>
                        </div>

                        <KeyboardPart heatMap={stats.errorMap} />
                    </div>
                    <footer className="c-card__footer">
                          {this.props.showLessonsButton &&
                          <button className="c-button c-button--brand fsa-button" onClick={this.onLessonsClick.bind(this)}>
                            <i className="fa fa-long-arrow-left"></i>
                            &nbsp;&nbsp; Back to Lessons
                          </button>
                          }

                          <button className="c-button c-button--brand fsa-button" onClick={this.props.restart}>
                            Retry Block&nbsp;&nbsp;
                            <i className="fa fa-repeat"></i>
                          </button>
                          { result.isLastBlock ? endButton : nextButton }
                    </footer>
                  </div>
            </div>
        );
    }
}