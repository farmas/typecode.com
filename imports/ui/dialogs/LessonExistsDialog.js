import React from 'react';
import Dialog from './Dialog';
import { FlowRouter } from 'meteor/kadira:flow-router';

export default class LessonExistsDialog extends Dialog {
    goToLesson() {
        if (this.props.lessonId) {
            FlowRouter.go('lesson', { lessonId: this.props.lessonId });
        }
    }

    getContent() {
        return (
            <div className="lessonExistsDialog">
                <div className="c-card">
                    <div className="c-card__body fsa-centered">
                        <h3 className="c-heading">A lesson with the url specified already exists.</h3>
                        <h3 className="c-heading fsa-italic">{this.props.url}</h3>
                    </div>
                    <footer className="c-card__footer">
                        <button className="c-button c-button--brand fsa-button" onClick={this.props.onCancel}>
                            Cancel&nbsp;&nbsp;
                            <i className="fa fa-times"></i>
                        </button>
                        <button className="c-button c-button--info fsa-button" onClick={this.goToLesson.bind(this)}>
                            Load Lesson&nbsp;&nbsp;
                            <i className="fa fa-long-arrow-right"></i>
                        </button>
                    </footer>
                  </div>
            </div>
        );
    }
}